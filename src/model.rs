use chrono::{DateTime, Utc};
use core::fmt::Debug;
use crate::CFG;
use yukikaze::client::{config::Config as TraitYukiConf, Request as YukiRequest};

#[derive(Clone, Debug, Deserialize)]
pub struct HelixPaginator {
    pub cursor: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct HelixWebhook {
    pub topic:      String,
    pub callback:   String,
    pub expires_at: DateTime<Utc>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct HelixWebhookReq {
    pub total:      usize,
    pub pagination: HelixPaginator,
    pub data:       Vec<HelixWebhook>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct HelixUser {
    pub id: String,
    pub login: String,
    pub display_name: String,
    #[serde(rename = "type")]
    pub user_type: String,
    pub broadcaster_type: String,
    pub description: String,
    pub profile_image_url: String,
    pub offline_image_url: String,
    pub view_count: usize,
    pub email: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct HelixStream {
    pub id: String,
    pub user_id: String,
    pub game_id: String,
    pub community_ids: Vec<String>,
    #[serde(rename = "type")]
    pub _type: String,
    pub title: String,
    pub viewer_count: usize,
    pub started_at: DateTime<Utc>,
    pub language: String,
    pub thumbnail_url: String,
}

pub type WebhookStreamPayload = PayloadContainer<HelixStream>;
pub type HelixUserPayload = PayloadContainer<HelixUser>;

#[derive(Clone, Debug, Deserialize)]
pub struct PayloadContainer<T>
where
    T: Debug,
{
    pub data: Vec<T>,
}

lazy_static! {
    static ref TMI_APP_ID_HEADER: yukikaze::header::HeaderValue =
        yukikaze::header::HeaderValue::from_str(&CFG.twitch.appid).expect("CLIENTID: Bad Format");
}

#[derive(Debug)]
pub struct YukiConf;

impl TraitYukiConf for YukiConf {
    fn default_headers(request: &mut YukiRequest) {
        use yukikaze::header;

        // Block to avoid having to type `request.headers_mut()` all the time.
        {
            let hd = request.headers_mut();
            hd.insert(
                header::USER_AGENT,
                header::HeaderValue::from_static(concat!("LiveNotif/", env!("CARGO_PKG_VERSION"))),
            );
            hd.insert(
                header::HeaderName::from_static("client-id"),
                TMI_APP_ID_HEADER.clone(),
            );
            hd.insert(
                header::ACCEPT_CHARSET,
                header::HeaderValue::from_static("utf-8"),
            );
            hd.insert(
                header::ACCEPT,
                header::HeaderValue::from_static("application/json"),
            );
        }

        ::yukikaze::client::config::DefaultCfg::default_headers(request);
    }
}
