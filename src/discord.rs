use crossbeam_channel::{unbounded, Sender};
use std::thread;
use yukikaze::{
    client::request::Request as YukiRequest,
    rt::{AutoClient, AutoRuntime},
};

fn str_clamp(s: &str, mlen: usize) -> String {
    if s.len() > mlen {
        String::from(&s[..mlen])
    } else {
        String::from(s)
    }
}

#[derive(Debug)]
pub struct WHMessage {
    pub hook:    String,
    pub message: String,
    pub username: String,
}



pub fn create_worker() -> Sender<WHMessage> {
    let (tx, rx) = unbounded::<WHMessage>();
    thread::spawn(move || {
        let _rt = yuki_rt_init!();
        while let Some(data) = rx.recv() {
            // TODO: Ratelimiting
            debug!("Recieved LIVE event, dispatching...");

            if let Ok(yk1) = YukiRequest::post(data.hook) {
                if let Ok(yk1) = yk1.json(&json! {
                    {
                        "content": str_clamp(&data.message, 1999),
                        "embeds": [
                            {
                                "title": "{{channelname}} - Twitch".replace("{{channelname}}", &data.username),
                                "url": "https://twitch.tv/{{channelname}}".replace("{{channelname}}", &data.username),
                                "color": 11659636,
                                "image": {
                                    "url": "https://static-cdn.jtvnw.net/previews-ttv/live_user_{{channelname}}-1280x720.jpg".replace("{{channelname}}", &data.username)
                                },
                                "author": {
                                    "name": "Twitch"
                                }
                            }
                        ]
                    }
                }) {
                    if let Err(e) = yk1.send().finish() {
                        warn!("Request errored: {:?}", e);
                    } else {
                        debug!("LIVE dispatch OK");
                    }
                }
            }
        }
    });

    tx
}
