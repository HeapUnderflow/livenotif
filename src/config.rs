use crate::CMDOPTS;
use rand::{thread_rng, Rng};
use std::{fs::File, io, path::Path};
use structopt::StructOpt;

pub fn load() -> io::Result<Config> {
    let mut cfg = load_file(&CMDOPTS.config)?;

    load_env(&mut cfg);
    load_cmdline(&mut cfg);

    Ok(cfg)
}

pub fn load_file(f: &str) -> io::Result<Config> {
    if !Path::new(f).exists() {
        let f = File::create(f)?;
        serde_yaml::to_writer(&f, &Config::generate()).expect("Write Failed!");
        return Err(io::Error::new(io::ErrorKind::Other, "Config missing"));
    }
    let f = File::open(f)?;
    let d = serde_yaml::from_reader::<_, Config>(&f).expect("Unable to unwrap");

    Ok(d)
}

fn load_env(c: &mut Config) {
    if let Some(port) = kankyo::key("LIVENOTIF_LISTEN_PORT") {
        match port.parse::<u16>() {
            Ok(o) => c.listen.port = o,
            Err(_) => warn!("Unable to parse port: ignoring"),
        }
    }

    if let Some(addr) = kankyo::key("LIVENOTIF_LISTEN_ADDR") {
        c.listen.addr = addr;
    }

    if let Some(hook) = kankyo::key("LIVENOTIF_WEBHOOK") {
        c.webhook.hook = hook;
    }

    if let Some(base) = kankyo::key("LIVENOTIF_BASEURL") {
        c.listen.baseurl = base;
    }
}

fn load_cmdline(c: &mut Config) {
    if let Some(ref port) = CMDOPTS.port {
        c.listen.port = port.clone();
    }

    if let Some(ref addr) = CMDOPTS.addr {
        c.listen.addr = addr.clone();
    }

    if let Some(ref hook) = CMDOPTS.hook {
        c.webhook.hook = hook.clone();
    }

    if let Some(ref base) = CMDOPTS.baseurl {
        c.listen.baseurl = base.clone();
    }
}

fn gen_rand_string(len: usize) -> String {
    static RAND_CHRS: &'static str =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!§$%&/()=?*+~#'.:,;|<>";

    lazy_static! {
        static ref CHRLST: Vec<char> = RAND_CHRS.chars().collect::<Vec<_>>();
    }

    let mut rng = thread_rng();
    let mut s = String::with_capacity(len);
    for _ in 0..len {
        s.push(rng.choose(&CHRLST).unwrap().clone());
    }
    s
}

// TODO: Add proper Argument hirachy and restructure Opts (CommandLine > EnvVar > ConfFile > Default (if present))

/// Create version of self (to be put into defaults, with defaults that are representative, not sensible)
pub trait Generate {
    /// Generate the Struct Instance
    fn generate() -> Self;
}

#[derive(Debug, StructOpt, Clone)]
#[structopt(about = "LiveNotify tool for Twitch")]
pub struct CommandLine {
    /// Config file to use
    #[structopt(short = "c", long = "config", default_value = "conf.yml")]
    pub config: String,

    /// Port to listen on [default: 8080]
    #[structopt(short = "p", long = "port")]
    pub port: Option<u16>,

    /// The address to listen on [default 127.0.0.1]
    #[structopt(short = "a", long = "addr")]
    pub addr: Option<String>,

    /// The webhook to use
    #[structopt(short = "w", long = "webhook", raw(aliases = r#"&["hook"]"#))]
    pub hook: Option<String>,

    /// The public baseurl
    #[structopt(short = "b", long = "baseurl")]
    pub baseurl: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub twitch:  Twitch,
    pub webhook: Webhook,
    pub listen:  Listen,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Twitch {
    pub appid:        String,
    pub user:         TwitchTargetUser,
    pub notification: TwitchNotification,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TwitchTargetUser {
    pub id:   Option<usize>,
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TwitchNotification {
    pub secret: String,
    pub salt:   String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Webhook {
    pub text: String,
    pub hook: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Listen {
    #[serde(default = "def_listen_addr")]
    pub addr: String,
    #[serde(default = "def_listen_port")]
    pub port: u16,
    pub baseurl: String,
    #[serde(default = "def_listen_lease")]
    pub lease_secs: usize,
}

fn def_listen_addr() -> String { String::from("127.0.0.1") }
fn def_listen_port() -> u16 { 8080 }
fn def_listen_lease() -> usize { crate::DEFAULT_LEASE_SECS }

impl Generate for Listen {
    fn generate() -> Listen {
        Listen {
            addr:       def_listen_addr(),
            port:       def_listen_port(),
            baseurl:    String::from("https://some.base/public/url/twitchevent"),
            lease_secs: def_listen_lease(),
        }
    }
}

impl Generate for Webhook {
    fn generate() -> Webhook {
        Webhook {
            text: String::from("hey, listen ! Its live !\nhttps://twitchtv./_xX_SuperCoolUser_Xx_"),
            hook: String::from("WEBHOOK_URL_HERE"),
        }
    }
}

impl Generate for TwitchNotification {
    fn generate() -> TwitchNotification {
        TwitchNotification {
            secret: gen_rand_string(25),
            salt:   gen_rand_string(25),
        }
    }
}

impl Generate for TwitchTargetUser {
    fn generate() -> TwitchTargetUser {
        TwitchTargetUser {
            id:   None,
            name: String::from("USERNAME_HERE (use id field instead if you can)"),
        }
    }
}

impl Generate for Twitch {
    fn generate() -> Twitch {
        Twitch {
            appid:        String::from("APPID_HERE"),
            user:         TwitchTargetUser::generate(),
            notification: TwitchNotification::generate(),
        }
    }
}

impl Generate for Config {
    fn generate() -> Config {
        Config {
            twitch:  Twitch::generate(),
            webhook: Webhook::generate(),
            listen:  Listen::generate(),
        }
    }
}
