#![feature(proc_macro_hygiene)]

#[macro_use]
extern crate tower_web;
#[macro_use]
extern crate utilities;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;

macro_rules! yuki_rt_init {
    () => {{
        use yukikaze::rt::init as yukinit;
        // Keep yuki alive as long as the inbound svr lives
        yukinit()
    }};
    (init) => {{
        use yukikaze::{client::Client as YukiClient, rt::init as yukinit};
        // Keep yuki alive as long as the inbound svr lives
        let ___rt = yukinit();

        let yukicl = YukiClient::<$crate::model::YukiConf>::new();
        yukikaze::rt::set(yukicl);
        ___rt
    }};
}

pub mod config;
pub mod discord;
pub mod model;

// use tokio::prelude::*;
use crossbeam_channel::Sender;
use hmac::{Hmac, Mac};
use hyper::{Response as HyperResponse, StatusCode};
use maud::{html, DOCTYPE};
use parking_lot::{Mutex, RwLock};
use sha2::{Digest, Sha256};
use std::{
    thread,
    time::{Duration, SystemTime},
};
use structopt::StructOpt;
use tower_web::{middleware::log::LogMiddleware, ServiceBuilder};
use url::Url;
use yukikaze::{
    client as ykclient,
    rt::{AutoClient, AutoRuntime},
};

type HmacSha256 = Hmac<Sha256>;

static HELIX_USER_URL: &'static str = "https://api.twitch.tv/helix/users";
static WEBHOOK_SUBSCRIBE_URL: &'static str = "https://api.twitch.tv/helix/webhooks/hub";
static DEFAULT_LEASE_SECS: usize = 21600;

#[derive(Debug)]
struct TwitchAcceptor {
    secret:           String,
    snd:              Sender<discord::WHMessage>,
    notifs_processed: RwLock<Vec<String>>,
    last_pushed:      Mutex<Option<SystemTime>>,
}

lazy_static! {
    static ref CMDOPTS: config::CommandLine = config::CommandLine::from_args();
}

lazy_static! {
    static ref JSON_DEFAULT: serde_json::Value = json!("!!INVALID_DATA!!");
    static ref CFG: config::Config = config::load().expect("fk");
}

#[derive(Debug, Response)]
#[web(status = "200")]
struct EmptyOkResponse;

#[derive(Debug, Response)]
#[web(status = "400")]
struct EmptyBadRequest;

impl TwitchAcceptor {
    fn new(unhashed_secret: &str, salt: &str, conn: Sender<discord::WHMessage>) -> TwitchAcceptor {
        TwitchAcceptor {
            secret:           format!(
                "{:x}",
                Sha256::digest(
                    &format!("{}+{}", unhashed_secret, salt)
                        .bytes()
                        .collect::<Vec<_>>(),
                )
            ),
            snd:              conn,
            notifs_processed: RwLock::new(Vec::default()),
            last_pushed:      Mutex::new(None),
        }
    }

    fn post_live(&self) {
        self.snd.send(discord::WHMessage {
            hook:    CFG.webhook.hook.clone(),
            message: CFG.webhook.text.clone(),
            username: CFG.twitch.user.name.clone()
        })
    }
}

impl_web! {
    impl TwitchAcceptor {
        #[get("/")]
        #[content_type("text/html")]
        fn hello_world(&self) -> Result<String, ()> {
            Ok(
                html! {
                    (DOCTYPE)
                    html {
                        head {
                            title {
                                "Base Page, you should not be here !"
                            }
                        }
                        body {
                            div style="width: 50%; margin-left: auto; margin-right:auto;" {
                                "Hello new person, this is the landin page of the twitch connector."
                                br;
                                "If you are here you are a naughty person"
                                br;
                                "go away"
                                br;
                                "nobody wants you to be here"
                                div style="font-size: 10px; font-family: monospace; margin-top: 10px;" {
                                    "Served by " (env!("CARGO_PKG_NAME")) "+v" (env!("CARGO_PKG_VERSION"))
                                }
                            }
                        }
                    }
                }.into_string()
            )
        }


        // Answering the VERIFY req here
        // Required for twitch webhooks to activate
        #[get("/event/live")]
        #[content_type("text/plain")]
        fn auth_verfiy(&self, query_string: String) -> Result<String, ()> {
            let eb_url = match Url::parse(&format!("http://not_a_real_url_.com/unreal?{}", query_string)) {
                Ok(o) => o,
                Err(e) => {
                    error!("Recieved invalid query params: {}", query_string);
                    error!("E {:?}", e);
                    return Err(());
                }
            };

            let mut challenge = String::new();
            'src: for (k, v) in eb_url.query_pairs() {
                if k == "hub.challenge" {
                    challenge = v.to_string();
                    break 'src;
                } else if k == "hub.mode" && v == "denied" {
                        warn!("The Subscribe request was denied !");
                        challenge = String::new();
                        break 'src;

                }
            };

            Ok(challenge)
        }


        #[post("/event/live")]
        #[allow(unused_variables)] // TODO Remove Warning Supression
        fn webhook_event(&self, x_hub_signature: String, body: Vec<u8>) -> Result<HyperResponse<&'static str>, HyperResponse<&'static str>> {
            let mut mac = HmacSha256::new_varkey(&self.secret.bytes().collect::<Vec<u8>>()).expect("Invalid Key");
            mac.input(&body);

            if let Ok(()) = mac.verify(&x_hub_signature.to_lowercase()[7..].bytes().collect::<Vec<u8>>()) {
                warn!("Recieved invalid notification hash !");
                return Err(HyperResponse::builder().status(StatusCode::BAD_REQUEST).body("").unwrap());
            }



            // TODO: Check if stream has gone online, or just changed status / game / etc.
            match serde_json::from_slice::<model::WebhookStreamPayload>(&body) {
                Ok(d) => {
                    if let Some(id) = d.data.into_iter().next() {

                        let vd = {
                            let r = self.notifs_processed.read();
                            id._type == "live" && !r.contains(&id.id)
                        };

                        if vd {
                            let mut mt = self.last_pushed.lock();
                            if let Some(md) = *mt {
                                if SystemTime::now().duration_since(md).expect("Time Failure") > Duration::from_secs(600) {
                                    self.post_live();
                                    self.notifs_processed.write().push(id.id);
                                    *mt = Some(SystemTime::now());
                                }
                            } else {
                                self.post_live();
                                self.notifs_processed.write().push(id.id);
                                *mt = Some(SystemTime::now());
                            }
                        } else {
                            debug!("Already processed notif with id {}", id.id);
                        }
                    }
                },
                Err(e) => {
                    warn!("{:?}", e); // 498516326097092628
                }
            }

            Ok(HyperResponse::builder().status(StatusCode::OK).body("").unwrap())
        }
    }
}

fn delayed_subscribe(duration: Option<Duration>) {
    let baseurl = CFG.listen.baseurl.clone();
    let uuid = CFG.twitch.user.id;
    let unme = CFG.twitch.user.name.clone();

    let sec = format!(
        "{:x}",
        Sha256::digest(
            &format!(
                "{}+{}",
                CFG.twitch.notification.secret, CFG.twitch.notification.salt
            )
            .bytes()
            .collect::<Vec<_>>(),
        )
    );

    thread::spawn(move || {
        thread::sleep(duration.unwrap_or_else(|| Duration::from_secs(1)));
        let _rt = yuki_rt_init!();

        let uid;

        if uuid.is_none() {
            // TODO: Query ID from API

            match ykclient::request::Request::get(format!("{}?login={}", HELIX_USER_URL, unme))
                .expect("Invalid URL Supplied")
                .empty()
                .send()
                .finish()
            {
                Ok(r) => {
                    if !r.is_success() {
                        error!("User resolve request failed");
                        error!("{:?}", r);
                        error!("B: {:?}", r.body().finish());
                        return;
                    }

                    let r = r
                        .json::<model::HelixUserPayload>()
                        .finish()
                        .expect("Unpack Failed")
                        .data
                        .into_iter()
                        .next();

                    if let None = r {
                        error!("Unable to find user `{}`", unme);
                        return;
                    }

                    let r = r.unwrap();

                    uid =
                        r.id.parse::<usize>()
                            .expect("Encountered invalid id in response");

                    // TODO Userid
                    info!("Resolved userid [->{}]", uid);
                },
                Err(e) => {
                    error!("Unable to resolve User");
                    error!("{:?}", e);
                    return;
                },
            }
        } else {
            uid = uuid.unwrap();
            info!("Using supplied userid [->{}]", uid);
        }

        info!("Subscribing to {} [{}]", unme, uid);

        // TODO: Force unsubscribe (so we dont max out or max-subscriptions)
        let subscribe_data = json! {{
            "hub.callback": format!("{}/event/live", baseurl),
            "hub.mode": "subscribe",
            "hub.topic": format!("https://api.twitch.tv/helix/streams?user_id={}", uid),
            "hub.lease_seconds": CFG.listen.lease_secs,
            "hub.secret": sec
        }};

        trace!("Payload: \n{:#?}", subscribe_data);

        match ykclient::request::Request::post(WEBHOOK_SUBSCRIBE_URL.to_string())
            .expect("Invalid URL Supplied")
            .json(&subscribe_data)
            .expect("Unable to serialize DATA")
            .send()
            .finish()
        {
            Ok(r) => {
                if !r.is_success() {
                    error!("Request Returned invalid response");
                    error!("\n{:#?}", r);
                    return;
                }
                info!("Subscribe payload successfully send");
            },
            Err(e) => {
                error!("Unable to send subscribe payload !");
                error!("{:?}", e);
                return;
            },
        }

        // Re-Subscribe one minute before the subscription runs out.
        // This should go fine, as we have 3 subscription slots for the same topic
        // One is as a reserve for unplanned crashes,
        // One as a "swap"
        // and the last one Expires 60 seconds after the new one has been requested, thus freeing swap.
        debug!(
            "Awating next subscribe in {} secs",
            CFG.listen.lease_secs - 60
        );
        delayed_subscribe(Some(Duration::from_secs(CFG.listen.lease_secs as u64 - 60)));
    });
}

fn main() {
    let e = kankyo::load();
    // Loginit
    loginit!()
        .app_level(match kankyo::key("LL") {
            Some(s) => match s.parse::<usize>() {
                Ok(0) => utilities::log::LevelFilter::Error,
                Ok(1) => utilities::log::LevelFilter::Warn,
                Ok(3) => utilities::log::LevelFilter::Debug,
                Ok(4) => utilities::log::LevelFilter::Trace,
                _ => utilities::log::LevelFilter::Info,
            },
            None => utilities::log::LevelFilter::Info,
        })
        .build()
        .expect("Unable to build Logging");

    if let Err(e) = e {
        warn!(".env error");
        warn!("{:?}", e);
    }

    let _dummy = CMDOPTS.port.clone();

    print_config();

    // Yuki
    let _rt = yuki_rt_init!(init);

    let ib = discord::create_worker();

    trace!("\n{:#?}", &*CFG);

    delayed_subscribe(None);
    // tower

    let addr = format!("{}:{}", CFG.listen.addr, CFG.listen.port)
        .parse()
        .expect("Unable to parse ADDRESS");

    info!("Listening ong http://{}", addr);
    ServiceBuilder::new()
        .resource(TwitchAcceptor::new(
            &CFG.twitch.notification.secret,
            &CFG.twitch.notification.salt,
            ib,
        ))
        .middleware(LogMiddleware::new(concat!(env!("CARGO_PKG_NAME"), "::web")))
        .run(&addr)
        .expect("Something Failed");
}

fn print_config() {
    let elips = |s: &str, l: usize| -> String {
        if s.len() > l {
            format!("{}...", &s[..l])
        } else {
            s.to_owned()
        }
    };

    debug!("### CONFIG START ###");

    debug!("# twitch");
    debug!("-> appid     : {}", elips(&CFG.twitch.appid, 4));
    debug!("-> user");
    debug!("|-> id       : {:?}", CFG.twitch.user.id);
    debug!("\\-> name     : {}", CFG.twitch.user.name);
    debug!("-> notification");
    debug!(
        "|-> secret   : {}",
        elips(&CFG.twitch.notification.secret, 5)
    );
    debug!(
        "\\-> salt     : {}",
        elips(&CFG.twitch.notification.salt, 5)
    );
    debug!("# webhook");
    debug!("-> text      : {:?}", CFG.webhook.text);
    debug!("-> hook      : {}", elips(&CFG.webhook.hook, 20));
    debug!("# listen");
    debug!("-> addr      : {}", CFG.listen.addr);
    debug!("-> port      : {}", CFG.listen.port);
    debug!("-> baseurl   : {}", CFG.listen.baseurl);
    debug!("-> lease_secs: {}", CFG.listen.lease_secs);

    debug!("### CONFIG END   ###");
}
