# Livenotif

A small micro-tool that waits for a stream to go live, and then posts a message to a webhook.

## What this is
A small tool. its dumb. its hacky. it only supports one channel

## What this is not
Something big. without flaws. scalable.

## How to set it up

You need:

- rust (nightly, tested on 1.31.0, othhers should work too)
- A twitch ClientID (from a twitch app)
- A target webhook
- A webserver that is publicly reachable (and "always on")

Then clone this repo and build the program

```sh
git clone https://github.com/HeapUnderfl0w/livenotif.git
cd livenotif
cargo build --release
```

after that run the program once (using `cargo run --release`) to generate the default config file.
Edit this file to your desires and then you can rerun it (using `cargo run --release`)`.


```yaml
---
# twitch related settings
twitch:
  # client id of the twitch app
  appid: APPID_HERE
  # user settings
  user:
    # user id to sub too (can also be the name)
    id: ~ 
    # username to use to lookup id
    name: "USERNAME_HERE (use id field instead if you can)"
  # Twitch notification secrets, used to sign messages. you should not need to change these,
  # if you generated the file using the program.
  # If you did not use the generation method but instead copied this file,
  # change both options below to each a random string of your choice.
  notification:
    secret: "VINk(1mb&.CF9B1*(3SUq3*NQ"
    salt: "5D12cWRRbb7#un1qcxPbe,65w"
# webhook settings
webhook:
  # the notification text to post
  text: "hey, listen ! Its live !\nhttps://twitchtv./_xX_SuperCoolUser_Xx_"
  # the url of the webhook to post to (it has to accept json in the form of {"content": "..."})
  hook: WEBHOOK_URL_HERE
# settings for the webhook connection server
listen: 
  # address of the builtin webserver to listen on
  addr: 127.0.0.1
  # port of the builtin webserver to listen on
  port: 8080
  # outward facing url of the server (or reverse proxy)
  # make shure the outward facing url of baseurl + /event/live is not caputred by any other route
  baseurl: "https://somecoolsize.com/twitchevents"
  # seconds a single subscription to twitch events is valid,
  # Do not set this value below 120 as it could lead to problems during resubscription
  #sSetting it to 0 will immediatly cancel the webhook and no events at all will be recieved
  lease_secs: 21600
```

## TODO's

- [ ] Check existing subs on startup (and remove if nessesary)
- [ ] Avoid unessesary lookup of UserID (use `&login` param instead
